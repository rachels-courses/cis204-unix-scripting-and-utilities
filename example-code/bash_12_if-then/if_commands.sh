#!/bin/bash

# if command
# then
#     commands
# fi

# if the command after "if" completes successfully (returns 0)
# then everything in "then" gets executed

echo "- pwd:"
if pwd
then
  echo "Valid command"
fi

echo ""
echo "- InvalidCommand:"
if InvalidCommand
then
  echo "Valid command"
fi

echo "The End"
