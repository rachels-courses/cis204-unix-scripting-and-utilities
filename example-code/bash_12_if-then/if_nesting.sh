#!/bin/bash

# if command
# then
#     commands
# else
#     commands
# fi

# if the command after "if" completes successfully (returns 0)
# then everything in "then" gets executed

echo ""
echo "- InvalidCommand:"
if InvalidCommand
then
  echo "Valid command"
else
  
  echo ""
  echo "- pwd:"
  if pwd
  then
    echo "Valid command"
  else
    echo "Invalid command"
  fi
fi

echo "The End"
