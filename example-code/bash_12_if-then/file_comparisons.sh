#!/bin/bash

# Let's see if certain files exist.
# -e : does item exist?
# -d : is item a directory?
# -f : is item a file?
# -r : exists, is readable?
# -s : exists, not empty?
# -w : exists, is writable?
# -x : exists, is executable?

filename="if_commands.sh"

if [ -r $filename ]; then
  echo "$filename is readable"
fi

if [ -s $filename ]; then
  echo "$filename is not empty"
fi

if [ -w $filename ]; then
  echo "$filename is writable"
fi

if [ -x $filename ]; then
  echo "$filename is executable"
fi

if [ -f $filename ]; then
  echo "$filename is a file"
fi
