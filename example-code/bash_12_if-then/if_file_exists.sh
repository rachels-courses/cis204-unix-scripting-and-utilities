#!/bin/bash

# Let's see if certain files exist.
# -e : does item exist?
# -d : is item a directory?
# -f : is item a file?

echo "----------------- EXAMPLE 1 -----------------"
filename="if_commands.sh"
echo "Does \"$filename\" exist?"

if [ -e $filename ]
then
  # Item exists
  echo "Item exists"
  
  echo ""
  echo "Is \"$filename\" a file?"
  
  if [ -f $filename ]
  then
    echo "Item is a file"
  elif [ -d $filename ]
  then
    echo "Item is a directory"
  else
    echo "Item is not a file or directory"
  fi
  
else
  # Item does not exist
  echo "Item does not exist"
fi


# Same code, this thing is a folder
echo ""
echo "----------------- EXAMPLE 2 -----------------"
filename="example_folder"
echo "Does \"$filename\" exist?"

if [ -e $filename ]
then
  # Item exists
  echo "Item exists"
  
  echo ""
  echo "Is \"$filename\" a file?"
  
  if [ -f $filename ]
  then
    echo "Item is a file"
  elif [ -d $filename ]
  then
    echo "Item is a directory"
  else
    echo "Item is not a file or directory"
  fi
  
else
  # Item does not exist
  echo "Item does not exist"
fi


# Same code, this thing doesn't exist
echo ""
echo "----------------- EXAMPLE 3 -----------------"
filename="borb"
echo "Does \"$filename\" exist?"

if [ -e $filename ]
then
  # Item exists
  echo "Item exists"
  
  echo ""
  echo "Is \"$filename\" a file?"
  
  if [ -f $filename ]
  then
    echo "Item is a file"
  elif [ -d $filename ]
  then
    echo "Item is a directory"
  else
    echo "Item is not a file or directory"
  fi
  
else
  # Item does not exist
  echo "Item does not exist"
fi
