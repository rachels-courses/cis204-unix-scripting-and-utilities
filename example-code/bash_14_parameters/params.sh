#!/bin/bash

echo $# total parameters

if [ $# -ne 2 ]; then
  echo "Need 2 parameters"
  exit 1
fi

echo $1
echo $2
