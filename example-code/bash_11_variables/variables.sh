#!/bin/bash

echo "User info: $USER"

# Creating variables: NO spaces allowed between VAR and = and VALUE!!

var1="Taco"
var2=1.29

echo "var1 is $var1"
echo "var2 is $var2"

dateResult=`date`
echo "Date Result is $dateResult"

pwdResult=$(pwd)
echo "Pwd Result is $pwdResult"
