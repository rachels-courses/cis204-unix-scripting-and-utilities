#!/bin/bash
# Generate thumbnails for images with a "thumbnail_" prefix

if [[ $# -ne 1 ]]; then
  echo "Expected parameter: Path"
  exit 1
fi

width=500
height=375

path="$1/"
if [[ $path =~ "//" ]]; then
  path="$1"
fi

echo "Generate thumbnails for path: $path"

for item in `ls $path`; do
  orig="$item"
  thum="thumbnail_$item"
  
  convert "$path$orig" -resize "$widthx$height" "$path$thum"
  
  echo "CREATE THUMBNAIL - ORIGINAL: [$orig] THUMBNAIL: [$thum]"
done
