echo "Commit message:"
#read message
message="U03E Starter Code"

echo "Operate on folders with:"
read class # cs250

for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "ADD, COMMIT, PUSH IN DIRECTORY: $f"
          cd $f
          git add .
          git commit -m "$message"
          git pull
          git push
          cd ..
        fi
    fi
done
