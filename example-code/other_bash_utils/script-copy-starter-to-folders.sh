#echo "Assignment path:"
#read path #/media/wilsha/Rachel/TEACHING/2022-01_SPRING/cs235-student/U03E_Templates_Exceptions_and_STL

path="/media/wilsha/Rachel/TEACHING/2022-01_SPRING/cs235-student/U03E_Templates_Exceptions_and_STL"

echo "Copy to folders with:"
read class # cs250

for f in *; do
    if [ -d "$f" ]; then
        #echo "----------------------------"
        
        
        if [[ "$f" == *"$class"* ]]; then
          echo "cp $path $f -r"
          cp $path $f -r
        fi
    fi
done
