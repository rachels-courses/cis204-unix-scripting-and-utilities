#!/bin/bash

echo "check-filesizes.sh filename"
echo "$# total parameters"

if [ $# -ne 1 ]; then
  echo "Need 1 parameters"
  exit 1
fi


filename=$1
dir=.

find $dir -name $filename | xargs du -sh $findtext
