echo "Operate on folders with (e.g., cs200):"
read class
#class="cs200"

echo "Assignment key: (e.g., U02E):"
read key
#key="U02E"

echo "Build (e.g., *.cpp):"
read build
#build="*.cpp"

echo "class=$class, key=$key, build=$build"

exePaths=()
codePaths=()
codeReportFile="$(pwd)/$class_$key.cpp"

echo "Code report file at... $codeReportFile"

rm $codeReportFile
touch $codeReportFile

# Iterate through folders, build each matching project item
for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then          
          cd $f
          
          if [ -d "$key" ]; then
            echo "$f/$key... BUILD"
            cd $key
            
            path=$f_$key.out
            
            g++ $build -o $path
            ls
            
            exePaths+=( "$f/$key/$path" )
            
            codefile=$(find . -name "*.cpp")
            
            for cf in ${codefile[@]}; do
              #echo "** FOUND FILE: $cf"
              echo "" >> $codeReportFile
              echo "// ----------------------------------------------------------------------" >> $codeReportFile
              echo "// FOLDER: $f, ASSIGNMENT: $key" >> $codeReportFile
              echo "// ----------------------------------------------------------------------" >> $codeReportFile
              cat $cf >> $codeReportFile
            done

            
            codePaths+=( "$f/$key/" )
            
            cd ..
          else
            echo "$f/$key... NOT FOUND"
          fi
          
          cd ..
        fi
    fi
done

#echo ""
#echo "AGGREGATE CODE INTO REPORT:"
#for c in ${codePaths[@]}; do
#  echo $c
#done


echo ""
echo "RUNNING PROGRAMS:"
# Display exe paths
for e in ${exePaths[@]}; do
  echo ""
  echo $e
  ./$e
done
