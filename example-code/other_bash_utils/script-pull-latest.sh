echo "Operate on folders with:"
read class # cs250

for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "STASH AND PULL: $f"
          cd $f
          git stash
          git pull
          cd ..
        fi
    fi
done
