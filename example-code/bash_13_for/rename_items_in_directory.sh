#!/bin/bash

# Give a directory as the argument, this will iterate through that directory,
# printing out all the files/folder names.

# for var in list
# do
#   commands
# done



sourceDir="example_folder"
targetDir="folder2"

echo $sourceDir
echo $targetDir

if [ -d $sourceDir ]; then
  echo "$sourceDir is a directory"
  
  if [ -r $sourceDir ]; then
    echo "$sourceDir is readable"
    
  else
    echo "$sourceDir is not readable"
    exit 1
  fi

else
  echo "$sourceDir is not a directory"
  exit 1

fi


if [ -d $targetDir ]; then
  echo "$targetDir is a directory"
  
  if [ -w $targetDir ]; then
    echo "$targetDir is writable"
    
  else
    echo "$targetDir is not writable"
    exit 1
  fi

else
  echo "$targetDir is not a directory"
  exit 1

fi

for item in `ls $sourceDir`
do
  frompath="$sourceDir/$item"
  topath="$targetDir/$item_copy"
  
  # 1. is it readable?
  # 2. is it a file? (not directory)
      
  if [ -f $sourceDir ]; then
  echo "$frompath is a file"
    
    if [ -r $sourceDir ]; then
      echo "$frompath is readable"
      
    else
      echo "$frompath is not readable"
      exit 1
    fi

  else
    echo "$frompath is not a file"
    exit 1

  fi
  
  echo "Move $frompath to $topath..."
  echo "" 
  
  if `cp $frompath $topath`; then
    echo "SUCCESS"
  else
    echo "FAILURE"
  fi
done


#directory="test_folder"

#echo "ls $directory ..."
#ls $directory

#echo ""
#echo "For loop..."
#for item in `ls $directory`
#do
  #path="$directory/$item"
  #topath="$directory/$item-test"
  
  #echo "Move $path to $topath..."
  
  #if `mv $path $topath`
  #then
    #echo "SUCCESS"
  #else
    #echo "FAILURE"
  #fi
#done
