#!/bin/bash

# Give a directory as the argument, this will iterate through that directory,
# printing out all the files/folder names.

# for var in list
# do
#   commands
# done

for item in `ls`
do
  echo "Item: $item"
done
